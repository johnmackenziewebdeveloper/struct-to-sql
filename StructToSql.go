package StructToSql

import (
	"fmt"
	"reflect"
	"strings"

	"github.com/jinzhu/inflection"
	"github.com/stoewer/go-strcase"
)

func GenerateCreateTableStatement(q interface{}) string {

	if reflect.ValueOf(q).Kind() == reflect.Struct {
		reflectionType := reflect.TypeOf(q)

		pluralizeTableName := FieldNameToSnakeCase(reflectionType.Name())

		query := fmt.Sprintf("create table %s (", pluralizeTableName)

		// set column names in sql insert
		for i := 0; i < reflectionType.NumField(); i++ {

			tagString, _ := reflectionType.Field(i).Tag.Lookup("mysql-type")

			if tagString == "" {
				continue
			}

			columnNameToSnakeCase := strcase.SnakeCase(reflectionType.Field(i).Name)
			columnNameToSnakeCase = strings.ToLower(columnNameToSnakeCase)

			if i == 0 {
				query = fmt.Sprintf("%s %s %s", query, columnNameToSnakeCase, tagString)
			} else {
				query = fmt.Sprintf("%s, %s %s", query, columnNameToSnakeCase, tagString)
			}
		}
		query = fmt.Sprintf("%s );\n", query)
		return query
	}
	return ""
}

func GenerateInsertStatement(q interface{}) string {

	if reflect.ValueOf(q).Kind() == reflect.Struct {

		reflectionType := reflect.TypeOf(q)
		// reflectionTypeKind := reflectionType.Kind()
		reflectionValue := reflect.ValueOf(q)
		pluralizeTableName := FieldNameToSnakeCase(reflectionType.Name())

		query := fmt.Sprintf("insert into %s (", pluralizeTableName)

		// set column names in sql insert
		for i := 0; i < reflectionValue.NumField(); i++ {

			columnNameToSnakeCase := strcase.SnakeCase(reflectionType.Field(i).Name)

			tagString, _ := reflectionType.Field(i).Tag.Lookup("mysql-type")

			if tagString == "" {
				continue
			}

			if i == 0 {
				query = fmt.Sprintf("%s%s", query, columnNameToSnakeCase)
			} else {
				query = fmt.Sprintf("%s, %s", query, columnNameToSnakeCase)
			}
		}
		query = fmt.Sprintf("%s) values (", query)

		// set values in sql insert
		for i := 0; i < reflectionValue.NumField(); i++ {
			switch reflectionValue.Field(i).Kind() {
			case reflect.Int:
				if i == 0 {
					query = fmt.Sprintf("%s%d", query, reflectionValue.Field(i).Int())
				} else {
					query = fmt.Sprintf("%s, %d", query, reflectionValue.Field(i).Int())
				}
			case reflect.String:
				if i == 0 {
					if reflectionValue.Field(i).String() == "NULL" {
						query = fmt.Sprintf("%s %s", query, reflectionValue.Field(i).String())
					} else {
						query = fmt.Sprintf("%s\"%s\"", query, reflectionValue.Field(i).String())
					}
				} else {
					if reflectionValue.Field(i).String() == "NULL" {
						query = fmt.Sprintf("%s, %s", query, reflectionValue.Field(i).String())
					} else {
						query = fmt.Sprintf("%s, \"%s\"", query, reflectionValue.Field(i).String())
					}
				}
			case reflect.Float32:
				if i == 0 {
					query = fmt.Sprintf("%s\"%f\"", query, reflectionValue.Field(i))
				} else {

					query = fmt.Sprintf("%s, \"%f\"", query, reflectionValue.Field(i))
				}
			case reflect.Float64:
				if i == 0 {
					query = fmt.Sprintf("%s\"%f\"", query, reflectionValue.Field(i))
				} else {

					query = fmt.Sprintf("%s, \"%f\"", query, reflectionValue.Field(i))
				}
			default:
				// fmt.Println("Unsupported type: " + reflectionValue.Field(i).Kind().String())
			}
		}

		query = fmt.Sprintf("%s );\n", query)

		return query
	}
	return ""
}
func OpenInsertStatement(q interface{}) string {

	if reflect.ValueOf(q).Kind() == reflect.Struct {
		reflectionType := reflect.TypeOf(q)

		pluralizeTableName := FieldNameToSnakeCase(reflectionType.Name())

		query := fmt.Sprintf("insert into %s (", pluralizeTableName)

		// set column names in sql insert
		for i := 0; i < reflectionType.NumField(); i++ {

			columnNameToSnakeCase := strcase.SnakeCase(reflectionType.Field(i).Name)
			columnNameToSnakeCase = strings.ToLower(columnNameToSnakeCase)

			tagString, _ := reflectionType.Field(i).Tag.Lookup("mysql-type")

			if tagString == "" {
				continue
			}

			if i == 0 {
				query = fmt.Sprintf("%s %s ", query, columnNameToSnakeCase)
			} else {
				query = fmt.Sprintf("%s, %s ", query, columnNameToSnakeCase)
			}
		}
		query = fmt.Sprintf("%s ) values\n", query)
		return query
	}
	return ""
}

func AddValuesToInsertStatement(q interface{}, isLast bool) string {

	if reflect.ValueOf(q).Kind() == reflect.Struct {

		// reflectionType := reflect.TypeOf(q)
		// reflectionTypeKind := reflectionType.Kind()
		reflectionValue := reflect.ValueOf(q)

		query := fmt.Sprintf("( ")

		// set values in sql insert
		for i := 0; i < reflectionValue.NumField(); i++ {
			switch reflectionValue.Field(i).Kind() {
			case reflect.Int:
				if i == 0 {
					query = fmt.Sprintf("%s%d", query, reflectionValue.Field(i).Int())
				} else {
					query = fmt.Sprintf("%s, %d", query, reflectionValue.Field(i).Int())
				}
			case reflect.String:
				if i == 0 {
					if reflectionValue.Field(i).String() == "NULL" {
						query = fmt.Sprintf("%s %s", query, reflectionValue.Field(i).String())
					} else {
						query = fmt.Sprintf("%s\"%s\"", query, reflectionValue.Field(i).String())
					}
				} else {
					if reflectionValue.Field(i).String() == "NULL" {
						query = fmt.Sprintf("%s, %s", query, reflectionValue.Field(i).String())
					} else {
						query = fmt.Sprintf("%s, \"%s\"", query, reflectionValue.Field(i).String())
					}
				}
			case reflect.Float32:
				if i == 0 {
					query = fmt.Sprintf("%s\"%f\"", query, reflectionValue.Field(i))
				} else {

					query = fmt.Sprintf("%s, \"%f\"", query, reflectionValue.Field(i))
				}
			case reflect.Float64:
				if i == 0 {
					query = fmt.Sprintf("%s\"%f\"", query, reflectionValue.Field(i))
				} else {

					query = fmt.Sprintf("%s, \"%f\"", query, reflectionValue.Field(i))
				}
			default:
				// fmt.Println("Unsupported type: " + reflectionValue.Field(i).Kind().String())
			}
		}

		if isLast {
			query = fmt.Sprintf("%s );\n", query)
		} else {
			query = fmt.Sprintf("%s ),\n", query)
		}

		return query
	}
	return ""
}

func FieldNameToSnakeCase(input string) string {
	input = strcase.SnakeCase(input)
	input = strings.Replace(input, "_", " ", -1)
	input = inflection.Plural(input)
	input = strcase.SnakeCase(input)
	return input
}
