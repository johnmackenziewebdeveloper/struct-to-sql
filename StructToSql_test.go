package StructToSql

import (
	"fmt"
	"testing"
	"time"
)

type Employee struct {
	id           int     `mysql-type:"INT(10)"`
	name         string  `mysql-type:"VARCHAR(255)"`
	address      string  `mysql-type:"VARCHAR(255)"`
	salary       int     `mysql-type:"VARCHAR(255)"`
	country      string  `mysql-type:"VARCHAR(255)"`
	longitude    float64 `mysql-type:"DECIMAL(10,7)"`
	latitude     float32 `mysql-type:"DECIMAL(10,7)"`
	createdAt    string  `mysql-type:"DATETIME"`
	randomNumber int     `mysql-type:"TINYINT"`
}

func TestCreateTable(t *testing.T) {

	expected := "create table employees ( id INT(10), name VARCHAR(255), address VARCHAR(255), salary VARCHAR(255), country VARCHAR(255), longitude DECIMAL(10,7), latitude DECIMAL(10,7), created_at DATETIME, random_number TINYINT );\n"

	e := Employee{}
	got := GenerateCreateTableStatement(e)

	if got != expected {
		t.Errorf("got %s \nexpected %s", got, expected)
	}
}

func TestInsertStatement(t *testing.T) {

	createdAt := time.Now().Format("2006-01-02 15:04:05")

	expected := "insert into employees (id, name, address, salary, country, longitude, latitude, created_at, random_number) values (565, \"Naveen\", \"Coimbatore\", 90000, \"India\", \"-1.558540\", \"53.812908\", \"" + createdAt + "\", 0 );\n"

	e1 := Employee{
		name:      "Naveen",
		id:        565,
		address:   "Coimbatore",
		salary:    90000,
		country:   "India",
		longitude: -1.5585400,
		latitude:  53.8129100,
		createdAt: createdAt,
	}
	got := GenerateInsertStatement(e1)

	if got != expected {
		t.Errorf("got %s \nexpected %s", got, expected)
	}
}

func TestOpenInsertStatement(t *testing.T) {

	employee := Employee{}

	expected := "insert into employees ( id , name , address , salary , country , longitude , latitude , created_at , random_number  ) values\n"
	outcome := OpenInsertStatement(employee)

	if outcome != expected {
		t.Errorf("got %s \nexpected %s", outcome, expected)
	}
}

func TestAllInsertFunctionality(t *testing.T) {

	createdAt := time.Now().Format("2006-01-02 15:04:05")

	expected := "insert into employees ( id , name , address , salary , country , longitude , latitude , created_at , random_number  ) values\n ( 565, \"Naveen\", \"Coimbatore\", 90000, \"India\", \"-1.558540\", \"53.812908\", \"" + createdAt + "\", 0 ),\n ( 565, \"Naveen\", \"Coimbatore\", 90000, \"India\", \"-1.558540\", \"53.812908\", \"" + createdAt + "\", 0 );\n"

	e1 := Employee{
		name:      "Naveen",
		id:        565,
		address:   "Coimbatore",
		salary:    90000,
		country:   "India",
		longitude: -1.5585400,
		latitude:  53.8129100,
		createdAt: createdAt,
	}

	outcome := OpenInsertStatement(e1)
	outcome = fmt.Sprintf("%s %s", outcome, AddValuesToInsertStatement(e1, false))
	outcome = fmt.Sprintf("%s %s", outcome, AddValuesToInsertStatement(e1, true))

	if outcome != expected {
		t.Errorf("got %s \nexpected %s", outcome, expected)
	}
}

func TestAddValuesToInsertStatement(t *testing.T) {

	employee := Employee{1, "john", "addr", 1000, "UK", 1.0000, 2.0000, "0000-00-00 00:00:00", 1234}

	expected := "( 1, \"john\", \"addr\", 1000, \"UK\", \"1.000000\", \"2.000000\", \"0000-00-00 00:00:00\", 1234 );\n"
	outcome := AddValuesToInsertStatement(employee, true)

	if outcome != expected {
		t.Errorf("got %s \nexpected %s", outcome, expected)
	}

	expected = "( 1, \"john\", \"addr\", 1000, \"UK\", \"1.000000\", \"2.000000\", \"0000-00-00 00:00:00\", 1234 ),\n"
	outcome = AddValuesToInsertStatement(employee, false)

	if outcome != expected {
		t.Errorf("got %s \nexpected %s", outcome, expected)
	}

}

type ExpectedOutcome struct {
	Input    string
	Expected string
}

func TestFieldNameToSnakeCase(t *testing.T) {

	namesToConv := []ExpectedOutcome{
		{"NodeStatus", "node_statuses"},
		{"Tenant", "tenants"},
		{"Gateway", "gateways"},
	}

	for _, test := range namesToConv {

		outcome := FieldNameToSnakeCase(test.Input)
		expected := test.Expected

		if outcome != expected {
			t.Errorf("got %s \nexpected %s", outcome, expected)
		}
	}
}
